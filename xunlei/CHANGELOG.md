### v3.20.2 (2024-08-30)
重要更新: 修复在某些极端情况下可能会删除挂载目录内容。 
受影响的版本有 v3.20.0 和 v3.20.1，请使用该版本的及时更新。 
### spk (2024-08-06)
for autobuild resource.
### v3.20.1 (2024-07-09)
修改一些权限的操作。
### v3.20.0 (2024-06-30)
版本号不再更随迅雷的版本。
### v3.11.2-3-4-g93c6346 (2024-05-23)
升级迅雷到v3.11.2
### v2.6.1(202204-20)
升级迅雷到v2.6.1
### v2.4.5(2022-03-16)
升级迅雷到v2.4.5
### v2.2.2(2022-01-08)
升级迅雷到v2.2.2
### v2.2.0(2022-01-01)
重构了安装器  
迅雷版本更新到v2.2.0  
1、支持独立账号密码登录  
2、磁盘空间显示0GB bug修复  
3、组队加速弹窗样式更新  
4、修复部分界面样式问题  
### v2.1.0(2021-12-17)
init
